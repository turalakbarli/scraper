<?php
require 'vendor/autoload.php';
$client = new \Goutte\Client();

$crawler = $client->request('GET', 'https://search.ipaustralia.gov.au/trademarks/search/advanced');
$form = $crawler->selectButton('Search')->form();
$crawler = $client->submit($form, ['wv[0]' => trim($trademark)]);

$result_count = $crawler->filter('.cf div div.results-count h2')->text();

//check pagination count
$per_page_result = 100;
$counter_page = $result_count/$per_page_result;
$pagination = (int)ceil($counter_page);
$pagination--;

$getLink = $crawler->getUri();

echo 'Result count:'.$result_count.PHP_EOL;
$dataArray = [];

for($i=0;$pagination>=$i;$i++)
{
    $crawler_2 = $client->request('GET',$getLink.'&p='.$i);
    $crawler_2->filter('table#resultsTable')->filter('tbody tr')->each(function ($tr) use (&$dataArray) {

            $dataTd =  $tr->filter('td');
            $dataTdCount = $dataTd->count();
            $indexing = 0;
            if($dataTdCount==7)
                $indexing = 1;

            $statuses = explode(':',$dataTd->eq(6-$indexing)->text());
            $status1 = str_replace('●', '', $statuses[0]??"");
            $status2 = str_replace('●', '', $statuses[1]??"");

            $imageLink = "---";
            if($indexing!=1)
                $imageLink = $dataTd->eq(3)->filter('img')->attr('src');

            $dataArray[$dataTd->eq(0)->text()]=array(
                'number'=>$dataTd->eq(2)->text(),
                'logo_url'=>$imageLink,
                'name'=>$dataTd->eq(4-$indexing)->text(),
                'classes'=>$dataTd->eq(5-$indexing)->text(),
                'status1'=>$status1!=""?trim($status1):"---",
                'status2'=>$status2!=""?trim($status2):"---",
                'details_page_url'=>$dataTd->eq(2)->filter('a')->attr('href')
            );

        });
}

print_r($dataArray);

